'''
Created on 15 Dec 2015

@author: YangKai
'''

from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello Python Flask!'

if __name__ == '__main__':
    app.run()